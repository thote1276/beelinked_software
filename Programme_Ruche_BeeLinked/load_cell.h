/* Sources : 
datasheet : https://cdn.sparkfun.com/assets/learn_tutorials/5/4/6/hx711F_EN.pdf
Inspiration : https://learn.sparkfun.com/tutorials/load-cell-amplifier-hx711-breakout-hookup-guide/all */


#define CALIBRATION_FACTOR      -20000 //11280.00 //This value is obtained using the SparkFun_HX711_Calibration sketch ////(-7050)
#define WEIGHT_MAX              40 //we can collect up to 40kg of honey in one hive 


