//Definition of download librairies

#include <SoftwareSerial.h> //C'est tchao
#include <Wire.h>
#include <ArduinoJson.h> // ???
#include <OneWire.h>
#include "HX711.h"
#include <Adafruit_AHTX0.h>
#include "arduinoFFT.h"



#include "config.h"
#include "DS18B20.h"
#include "load_cell.h"
#include "humidity_sensor.h"
#include "frequency.h"

//Definition of objects related to previous librairies
HX711 load_cell;
OneWire DS18B20_rows(BUS_ONE_WIRE);
Adafruit_AHTX0 aht;
arduinoFFT FFT = arduinoFFT(); /* Create FFT object */

void setup()
{
#ifdef DEBUG_SERIAL
  Serial.begin(9600);
  Serial.println("Initializing..........");
#endif //debug serial

  setup_temperature();
  setup_HX711();
  setup_AHT20();
}

void loop()
{
  float temperature_inside[7]; // = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  float weight = 0.0;
  sensors_event_t temp;
  sensors_event_t hum;
  float frequency = 0.0;

  while (1)
  {
    read_DS18B20_array(temperature_inside);
    read_weight(&weight);
    read_humidity(temp, hum);
    read_mean_frequency(&frequency);
    //envoyer par LORA

    delay(1693000);
  }
}

/*===================================================================================
 * Humidity sensor
 ===================================================================================*/
void setup_AHT20()
{
  aht.begin();
#ifdef DEBUG_SERIAL
  Serial.println("AHT20, the humidity sensor correctly iniatilize");
#endif //DEBUG_SERIAL
}

void read_humidity(sensors_event_t hum, sensors_event_t temp)
{
  aht.getEvent(&hum, &temp);
}

/*===================================================================================
 * Load cell sensor
 ===================================================================================*/
void setup_HX711()
{
  load_cell.begin(PIN_DOUT_HX711, PIN_CLK_HX711);
  load_cell.set_scale(CALIBRATION_FACTOR); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  load_cell.tare();                        //Assuming there is no weight on the scale at start up, reset the scale to 0

#ifdef DEBUG_SERIAL
  Serial.println("HX711 and its sensors correctly iniatilize");
#endif //DEBUG_SERIAL
}

void read_weight(float *weight)
{
  float l_weight;
  while (true)
  {
    l_weight = load_cell.get_units();
    if (l_weight > 0 and l_weight < WEIGHT_MAX)
      (*weight) = l_weight;
    break;
  }
}

/*===================================================================================
 * Temperature sensor
 ===================================================================================*/
void setup_temperature()
{
#ifdef DEBUG_SERIAL
  Serial.println("Temperature OK");
#endif //debug serial
}

//Each DS18B20 has it own address, we should know him in order to speak with the right one
float getTemperature(const byte addr[])
{
  int l_data[9];

  // To begin a data transfer we need to reset the bus
  DS18B20_rows.reset();
  // ROM command to select the right sensors
  DS18B20_rows.select(addr);
  //ROM command to start a temperature conversion
  DS18B20_rows.write(CONVERT_T, 1);
  delay(800);

  // To begin a data transfer we need to reset the bus
  DS18B20_rows.reset();
  // ROM command to select the right sensors
  DS18B20_rows.select(addr);
  //ROM command to send what is stored in registers
  DS18B20_rows.write(READ_SCRACHPAD);

  /* Reading of the incoming bytes */
  for (int l_i = 0; l_i < 9; l_i++)
  {
    l_data[l_i] = DS18B20_rows.read();
  }

  /* In the scratchpad, only the two first byte are relevant */
  return (int16_t)((l_data[1] << 8) | l_data[0]) * 0.0625;
}

void read_DS18B20_array(float *tab)
{
  for (int l_i = 0; l_i < 7; l_i++)
  {
    tab[l_i] = getTemperature(DS18B20_ADDRESS[l_i]);
  }
}

/*===================================================================================
 * FFT
 ===================================================================================*/
void setup_FFT()
{
  TIMSK0 = 0;    // turn off timer0 for lower jitter
  ADCSRA = 0xe5; // set the adc to free running mode
  ADMUX = 0x40;  // use adc0
  DIDR0 = 0x01;  // turn off the digital input for adc0
#ifdef DEBUG_SERIAL
  Serial.println("FFT ready");
#endif //debug serial
}

float FFT_one_value()
{
  /* Build raw data */
  for (uint16_t i = 0; i < nb_samples; i++)
  {
    while (!(ADCSRA & 0x10))
      ;            // wait for adc to be ready
    ADCSRA = 0xf5; // restart adc
    byte m = ADCL; // fetch adc data
    byte j = ADCH;
    int k = (j << 8) | m; // form into an int
    k -= 0x0200;          // form into a signed int
    k <<= 6;              // form into a 16b signed int
    vReal[i] = k;         // put real data into even bins
    vImag[i] = 0.0;       //Imaginary part must be zeroed in case of looping to avoid wrong calculations and overflows
  }
  /* Print the results of the simulated sampling according to time */
  FFT.Windowing(vReal, nb_samples, FFT_WIN_TYP_HAMMING, FFT_FORWARD); /* Weigh data */
  FFT.Compute(vReal, vImag, nb_samples, FFT_FORWARD);                 /* Compute FFT */
  FFT.ComplexToMagnitude(vReal, vImag, nb_samples);                   /* Compute magnitudes */
  double x;
  double v;
  FFT.MajorPeak(vReal, nb_samples, samplingFrequency, &x, &v);
  return x;
}

void read_mean_frequency(float *val)
{
  float frequency[NB_VALUE];
  float comparaison;
// register multiple values
  for (int l_j = 0; l_j < NB_VALUE; l_j++)
  {
    frequency[l_j] = FFT_one_value();
  }
  //order the frequency stored and take the value in the middle
  for (int l_i = 0; l_i < NB_VALUE - 1; l_i++)
  {
    for (int l_j = l_i + 1; l_j < NB_VALUE; l_j++)
    {
      if (frequency[l_i] > frequency[l_j])
      {
        comparaison = frequency[l_i];
        frequency[l_i] = frequency[l_j];
        frequency[l_j] = comparaison;
      }
    }
  }
  (*val) = frequency[NB_VALUE/2];
}
