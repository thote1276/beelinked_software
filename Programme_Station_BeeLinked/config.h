#define UNO
#define DEBUG_SERIAL

#ifdef UNO

//Differents sensors attached to a hive

//DS18B20
#define BUS_ONE_WIRE        2 //each temperature sensors will be attached to the same wire and communicate with an oneWire

//HX711 use a 24 bits ADC, needs a clk to sample the conversion, and a bus for the output
#define PIN_CLK_HX711       13
#define PIN_DOUT_HX711      11


//About SIM configuration
#define PIN_POWER_SIM 9
#define PIN_RX_SIM    7
#define PIN_TX_SIM    8



#endif //UNO
