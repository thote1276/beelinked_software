//Definition of download librairies

#include <SoftwareSerial.h> //C'est tchao
#include <Wire.h>
#include <ArduinoJson.h> // ???
#include <Adafruit_AHTX0.h>
#include <Adafruit_BMP085.h>
#include <String.h>

#include "config.h"
#include "humidity_sensor.h"


//Definition of objects related to previous librairies

Adafruit_AHTX0 aht;
Adafruit_BMP085 bmp180;
SoftwareSerial sim(PIN_RX_SIM, PIN_TX_SIM);

void setup()
{
#ifdef DEBUG_SERIAL
  Serial.begin(9600);
  Serial.println("Initializing..........");
#endif //debug serial

  setup_AHT20();
  setup_pressure();
}

void loop()
{

  float temperature;
  float humidity;
  double pressure, temperature1;

  while (1)
  {
    read_humidity(&temperature, &humidity);
    read_pressure(&temperature1, &pressure);
    //recevoir par Lora,
    //envoyer par GPRS


    delay(1693000);
  }
}

/*===================================================================================
   Humidity sensor
  ===================================================================================*/
void setup_AHT20()
{
  aht.begin();
#ifdef DEBUG_SERIAL
  Serial.println("AHT20, the humidity sensor correctly iniatilized");
#endif //DEBUG_SERIAL
}

void read_humidity(float *hum, float *temp)
{
  sensors_event_t l_hum, l_temp;
  aht.getEvent(&l_hum, &l_temp);
  (*temp) = l_temp.temperature;
  (*hum) = l_hum.relative_humidity;
}

/*===================================================================================
   Pressure sensor
  ===================================================================================*/

void setup_pressure() {

  bmp180.begin();
#ifdef DEBUG_SERIAL
  Serial.println("BMP180, the pressure sensor correctly iniatilized");
#endif //DEBUG_SERIAL
}

void read_pressure(double *temp, double *pres) {
  (*temp) = bmp180.readTemperature();
  (*pres) = bmp180.readPressure();
}


void GSM_communication()
{
   char message[256];
  wake_sleep_sim();

  sim.println("AT"); //test 
  sim.println("AT+SAPBR=3,1,\"Contype\",\"GPRS\""); //configuration GPRS
  sim.println("AT+CMEE=2\r"); //print error
  sim.println("AT+SAPBR=3,1,\"APN\",\"free\"");// APN
  sim.println("AT+SAPBR=1,1"); // connection try
  sim.println("AT+SAPBR=2,1");  //verification connection
  // sim.println("AT+HTTPTERM"); 
  sim.println("AT+HTTPINIT");
  sim.println("AT+HTTPPARA=\"CID\",1");
 // snprintf (message, sizeof(message), "AT+HTTPPARA=\"URL\",\"http://82.225.118.161:16800/wordpress/getSonde.php?Json={\\\"Ruche1\\\":[\\\"%d\\\",\\\"%d\\\",\\\"%d\\\",\\\"%d\\\",\\\"%d\\\"]}\"",int(humidity), int(temperature), int(tempC1), int(tempC2), int((poids-7.65)));
 // sim.println(message); //Server address
 // sim.println("AT+HTTPACTION=0"); //start a session
 // sim.println("AT+HTTPREAD"); // lecture Http UTILE ???
  sim.println("AT+HTTPTERM"); //fin http
  
  wake_sleep_sim();

}

void wake_sleep_sim() {
  digitalWrite(PIN_POWER_SIM, HIGH);    //sending a key_pwm according to the datasheet
  delay(5000);              
  digitalWrite(PIN_POWER_SIM, LOW);
  delay(10000);
}

boolean ShowSerialData()
{
 String message = "";
  while (sim.available()){
   message.concat(char((sim.read())));  //Forward what Software Serial received to Serial Port
  }
  return check_acknowledge(message);
}

boolean check_acknowledge(String message) {
  if (message.indexOf("OK") > 0) {
    return true;
  }
  else {
    return false;
  }
}
